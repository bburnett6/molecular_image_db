
# Molecular Image Database

## Basic usage

### _Recommended_ Docker Compose:

Currently, the database is hosted in an sqlite file mounted in `/data` via a volume within the `docker-compose.yaml` file. In future versions, this may change (especially for the core scientific data database, not so much the user database). Without any modifications to the project, a `data` directory will be created at `./data` and is mounted to `/data` within the container. databases `./data/{users,data}.db` will then be created. These default settings can be run with
```
docker-compose up --build
```
You can then access the API using tools like `httpie`:
```
#set the first user
http post hostname:9000/api/users username=test password=coolpass
#get the api token
http --auth test:coolpass post hostname:9000/api/tokens
```

### Dockerless:

Running without Docker will create and store the projects databases in `dbapp/{users,data}.db`. This will also create a bunch of artifacts related to the project (including a `migrations` directory and `__pycache__` directories). Running with the default settings can be done with
```
./server.py
```

### Development notes

I typically develop the frontend with

```
docker-compose up --build #stand up the service
docker-compose down -v #take down the service and all its volumes
git fetch --all
git reset --hard origin/master
```

this will maintain the database state while pulling all changes to the frontend. When changes are made to the API, the database will likely be okay if anything other than `models.py` is changed. If `models.py` is changed, there is code to automatically migrate, but it is not guaranteed to work. 
