
//import React, { useState } from 'react';
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Login from './components/Login/Login';
import useToken from './utils/useToken';
import Datasets from './components/Datasets/Datasets';
import Settings from './components/Settings/Settings';

import './App.css';

function App() {
	const { token, setToken, getToken } = useToken();
	const appDomain = "http://10.0.0.191:3000";

	if (!token) {
		return <Login setToken={setToken} />
	}

	return (
		<div>
			<div id="nav-bar">
				<h1>My App!</h1>
				<a href={appDomain + "/"}>Home</a>
				<a href={appDomain + "/datasets"}>Datasets</a>
				<a href={appDomain + "/settings"}>Settings</a>
			</div>
			<BrowserRouter>
				<Routes>
					<Route exact path="/datasets" element={<Datasets tokenGetter={getToken} />} />
					<Route exact path="/settings" element={<Settings tokenGetter={getToken} />} />
				</Routes>
			</BrowserRouter>
		</div>
	);
}

export default App;