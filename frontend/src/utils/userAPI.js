
/*
 Functions for handling API calls related to users
 */

export async function updateUser(authToken, user) {
	//TODO: add catch to fetch
	const response = await fetch(user._links.self, {
		method: 'PUT',
		headers: {
			'Content-type': 'application/json',
			'Authorization': 'Bearer ' + authToken.token,
		},
		body: JSON.stringify(user),
	});

	return response.json();
}

export async function getAllUsers(authToken, url) {
	//TODO: add catch to fetch
	const response = await fetch(url, {
		method: 'GET',
		headers: {
			'Content-type': 'application/json',
			'Authorization': 'Bearer ' + authToken.token,
		},
	});

	return response.json();
}

export async function getCurrentUser(token) {
	//TODO: add catch to fetch
	const response = await fetch('/api/users/' + token.uid, {
      method: 'GET', 
      headers: {'Authorization': 'Bearer ' + token.token}
    });
    
    return response.json();
}