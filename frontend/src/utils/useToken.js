
import { useState } from 'react';

/*
I realize this says token, but it is really an object of shape
{
	"uid": id,
	"token": token,
}
Sorry in advance for any confusion. 
*/
export default function useToken() {
	const retrieveToken = () => {
		const tokenString = localStorage.getItem('token');
		const userToken = JSON.parse(tokenString);
		return userToken
	};
	const [token, setToken] = useState(retrieveToken());

	const saveToken = userToken => {
		localStorage.setItem('token', JSON.stringify(userToken));
		setToken(userToken.token);
	}

	return {
		getToken: retrieveToken,
		setToken: saveToken,
		token
	}
}