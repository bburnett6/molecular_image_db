
import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './Login.css';

async function loginUser(uname, passwd) {
	//TODO: add catch to response
	return fetch('/api/tokens', {
      method: 'POST', 
      headers: {'Authorization': 'Basic ' + btoa(uname + ':' + passwd)}
    })
    .then(response => response.json())
}

function LoginForm({ tokenSetter }) {
	const [username, setUsername] = useState();
	const [password, setPassword] = useState();

	const handleSubmit = async e => {
		e.preventDefault();
		const token = await loginUser(username, password);
		//console.log('Got token:');
		//console.log(token);
		tokenSetter(token);
	};

	return (
		<div className="login-form"> 
			<h1>Log In!</h1>
			<form onSubmit={handleSubmit}>
				<label>
					<p>Username</p>
					<input type="text" onChange={e => setUsername(e.target.value)} />
				</label>
				<label>
					<p>Password</p>
					<input type="password" onChange={e => setPassword(e.target.value)} />
				</label>
				<div>
					<button type="submit">Submit</button>
				</div>
			</form>
		</div>
	);
}

LoginForm.propTypes = {
	tokenSetter: PropTypes.func.isRequired
}

async function registerUser(uname, passwd) {
	//TODO: Add catch to response
	return fetch('/api/users', {
      method: 'POST', 
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
      	username: uname,
      	password: passwd,
      })
    })
    .then(response => response.json())
}

function RegisterForm({ registerSuccess }) {
	const [username, setUsername] = useState();
	const [password, setPassword] = useState();
	const [confirmPass, setConfirmPass] = useState();

	const handleSubmit = async e => {
		e.preventDefault();
		if (password === confirmPass) {
			const response = await registerUser(username, password);
			registerSuccess();
			console.log('Got response:');
			console.log(response);
		}
	};

	return (
		<div className="register-form"> 
			<h1>Register!</h1>
			<form onSubmit={handleSubmit}>
				<label>
					<p>Username</p>
					<input type="text" onChange={e => setUsername(e.target.value)} />
				</label>
				<label>
					<p>Password</p>
					<input type="password" onChange={e => setPassword(e.target.value)} />
				</label>
				<label>
					<p>Confirm Password</p>
					<input type="password" onChange={e => setConfirmPass(e.target.value)} />
				</label>
				<div>
					<button type="submit">Submit</button>
				</div>
			</form>
		</div>
	);
}

RegisterForm.propTypes = {
	registerSuccess: PropTypes.func.isRequired
}

export default function Login({ setToken }) {
	const [isRegister, setRegister] = useState(false);

	function toggleRegister() {
		setRegister(!isRegister);
	}

	function registerSuccess() {
		setRegister(false);
	}

	return (
		<div className="login-wrapper">
			{isRegister ? (
				<RegisterForm registerSuccess={registerSuccess} />
			) : (
				<LoginForm tokenSetter={setToken} />
			)}
			<button onClick={toggleRegister} > 
				{isRegister ? 'Login' : 'Register'}
			</button>
		</div>
	);
}

Login.propTypes = {
	setToken: PropTypes.func.isRequired
}