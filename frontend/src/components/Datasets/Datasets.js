
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { getCurrentUser } from '../../utils/userAPI';
import './Datasets.css';

async function getDatasetNames(authToken) {
	//TODO: add catch to fetch
	const response = await fetch('/api/data', {
		method: 'GET',
		headers: {
			'Content-type': 'application/json',
			'Authorization': 'Bearer ' + authToken.token,
		},
	});

	return response.json();
}

function DatasetSelector({ token, datasetSetter }) {

	const [datasetList, setDatasetList] = useState([]);
	const [selectValue, setSelectValue] = useState("Select One")

	useEffect(() => {
		getDatasetNames(token)
		.then((data) => setDatasetList(["Select One", ...data.datasets]))
	}, []);

	function handleSubmit(e) {
		e.preventDefault();
		if (selectValue !== "Select One") {
			datasetSetter(selectValue);
		}
	}

	return (
			<form onSubmit={handleSubmit}>
				<label>
					Select Dataset:{' '}
					<select value={selectValue} onChange={e => setSelectValue(e.target.value)}>
						{datasetList.map((dataset, index) => 
							<option key={index} value={dataset}>{dataset}</option>
							)}
					</select>
				</label>
				<button type="submit">Query</button>
			</form>
		)
}

function DatasetItem({ token, item }) {
	const [itemState, setItemState] = useState(item);
	const [oldItemState, setOldItemState] = useState(item); //when we exit edit without updating, revert back to old state
	const [isEdit, setIsEdit] = useState(false);

	const handleChange = ({ target }) => {
		setItemState({
			...itemState,
			[target.id]: target.value
		});
	}

	function handleEditClick() {
		if (isEdit) {
			setItemState(oldItemState);
		}
		setIsEdit(!isEdit);
	}

	function handleSubmit(e) {
		e.preventDefault();
		//when post gets implemented don't forget to propagate changes to oldItemState too
		//setOldItemState(itemState);
		console.log("TODO: POST update to API.");
	}

	//map the object for display
	const itemValues = Object.entries(itemState).filter(([key, value]) => {
		if (key === "id" || key[0] === "_") {
			return false; //skip showing id. Skip showing meta entries that begin with "_" 
		} else {
			return true;
		}
	}).map(([key, value]) => {
		return (
			<div key={key}>
				<p>{key} : </p>
				<p>{value}</p>
			</div>
		);
	});

	//map the object entries to inputs for updating
	const itemInputs = Object.entries(itemState).filter(([key, value]) => {
		if (key === "id" || key[0] === "_") {
			return false; //No updating id or meta entries that begin with "_" 
		} else {
			return true;
		}
	}).map(([key, value]) => {
		return (
			<label key={key}>
				<p>{key}</p>
				<input id={key} value={value ? value.toString() : ""} onChange={handleChange} />
			</label>
		);
	});

	if (!isEdit) {
		return (
			<div>
				<strong>Entry {item.id} </strong>
				<button onClick={handleEditClick}>
					Edit Item
				</button>
				<div className="item-view">
					{ itemValues }
				</div>
			</div>
		);
	} else {
		return (
			<div>
				<strong>Entry {item.id} </strong>
				<button onClick={handleEditClick}>
					Exit Edit 
				</button>
				<form onSubmit={handleSubmit}>
					<div className="item-view-edit">
						{ itemInputs }
					</div>
					<br />
					<button type="submit">Update Item</button>
				</form>
			</div>
		);
	}
}

async function getDataset(authToken, datasetEndpoint) {
	//TODO: Add catch to fetch
	const response = await fetch(datasetEndpoint, {
		method: 'GET',
		headers: {
			'Content-type': 'application/json',
			'Authorization': 'Bearer ' + authToken.token,
		},
	});

	return response.json();
}

function Dataset({ token, dataset }) {

	const [datasetEntries, setDatasetEntries] = useState([]);
	const [datasetLinks, setDatasetLinks] = useState({"next": null, "prev": null, "self": null});

	useEffect(() => {
		getDataset(token, '/api/data/' + dataset)
		.then((data) => {
			setDatasetEntries(data.items);
			setDatasetLinks(data._links);
		});
	}, [dataset]); //Dataset effect will trigger when `dataset` changes

	function handleNavNext() {
		getDataset(token, datasetLinks.next)
		.then((data) => {
			setDatasetEntries(data.items);
			setDatasetLinks(data._links);
		});
	}

	function handleNavPrev() {
		getDataset(token, datasetLinks.prev)
		.then((data) => {
			setDatasetEntries(data.items);
			setDatasetLinks(data._links);
		});
	}

	const entryList = datasetEntries.map((item, index) => 
		<DatasetItem key={index.toString()} token={token} item={item} />
		);

	return (
			<div className="dataset-view">
				<h3>{ dataset }</h3>
				<div className="dataset-entry-wrapper">
					{ entryList }
				</div>
				<div className="dataset-nav">
					{datasetLinks.prev ? (
						<button onClick={handleNavPrev}>Previous</button>
						) : (
						<button>No Previous</button>
					)}
					{datasetLinks.next ? (
						<button onClick={handleNavNext}>Next</button>
						) : (
						<button>No Next</button>
					)}
				</div>
			</div>
		);
}

export default function Datasets({ tokenGetter }) {
	const userToken = tokenGetter();

	const [currentUser, setCurrentUser] = useState(null);
	const [dataset, setDataset] = useState(null); //Name of dataset to view

	useEffect(() => {
		getCurrentUser(userToken)
		.then((data) => setCurrentUser(data));
	}, []);

	if (currentUser) {
		return (
			<div className="datasets-wrapper">
				<DatasetSelector token={userToken} datasetSetter={setDataset} />
				{dataset ? (
					<Dataset token={userToken} dataset={dataset}/> 
					) : (
					<p>Select Dataset to view</p>
				)}
			</div>
		);
	} else {
		//authorization by api requires a fetch, and fetches need to be
		//within reacts useEffect. This if/else basically tells react to
		//wait until the user is authorized, and until then render nothing.
		return null;
	}
}

Datasets.propTypes = {
	tokenGetter: PropTypes.func.isRequired
}