
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { updateUser, getAllUsers, getCurrentUser } from '../../utils/userAPI';
import './Settings.css';

function UserInfo({ user }) {

	return (
		<div className="user-info"> 
			<h1>Hello {user.username}!</h1>
			{user.edit_permission ? (
				<p>You are an editor</p>
			) : (
				<p>You do not have edit permission</p>
			)}
			{user.is_admin && <p>Looks like you're an admin!</p>}
		</div>

	);
}

function AdminUpdateUser({ authToken, user }) {

	const [currUser, setCurrUser] = useState(user);

	const handleSubmit = async e => {
		e.preventDefault();
		const resp = await updateUser(authToken, currUser);
	};

	const handleAdminChange = () => {
		let setting = currUser.is_admin === 'True' ? 'False' : 'True';
		setCurrUser({
			...currUser,
			is_admin: setting,
		});
	};

	const handleEditorChange = () => {
		let setting = currUser.edit_permission === 'True' ? 'False' : 'True';
		setCurrUser({
			...currUser,
			edit_permission: setting,
		});
	};

	return (
		<div> 
			<h3>{user.username}</h3>
			<form onSubmit={handleSubmit}>
				<input type="checkbox" 
					checked={currUser.is_admin === 'True'} 
					onChange={handleAdminChange} 
				/> User is admin (can manage other users)
				<input type="checkbox" 
					checked={currUser.edit_permission === 'True'} 
					onChange={handleEditorChange}
				/> User is editor (can manage the database)
				<button type="submit">Update User Permissions</button>
			</form>
		</div>
	);
}

function AdminUserView({ token }) {
	
	const [usersView, setUsersView] = useState(null);

	useEffect(() => {
		getAllUsers(token, '/api/users')
		.then((data) => setUsersView(data));
	}, []);

	/*
	usersView is a json object given by:
	{
		_links: {next: urlStr, prev: urlStr, self: urlStr},
		_meta: {page: int, per_page: int, total_items: int},
		items: [
				{
					_links: {self: urlStr},
					edit_permission: boolStr,
					is_admin: boolStr,
					uid: int,
					username: str,
				}, ...
			]
	}
	*/

	if (usersView) {
		const userList = usersView.items.map((user) => 
			<AdminUpdateUser key={user.uid.toString()} authToken={token} user={user} />
			);

		return (
			<div className="admin-user-view">
				{ userList }
				{
					usersView._links.next && <button 
						onClick={() =>
							getAllUsers(token, usersView._links.next)
							.then((data) => setUsersView(data))
						}>Next
						</button> 
				}
				{ 
					usersView._links.prev && <button 
					onClick={ () =>
						getAllUsers(token, usersView._links.prev)
						.then((data) => setUsersView(data))
					}>Previous
					</button> 
				}
			</div>
		); 
	} else {
		return null;
	}
}

export default function Settings({ tokenGetter }) {
	const userToken = tokenGetter();

	const [currentUser, setCurrentUser] = useState(null);

	useEffect(() => {
		getCurrentUser(userToken)
		.then((data) => setCurrentUser(data));
	}, []);
	
	if (currentUser) {
		return (
			<div className="settings-wrapper">
				<UserInfo user={currentUser} />
				{currentUser.is_admin ? (
					<AdminUserView token={userToken} /> 
					) : ( 
					<h1>Hi</h1>
				)}
			</div>
		);
	} else {
		return null;
	}
}

Settings.propTypes = {
	tokenGetter: PropTypes.func.isRequired
}