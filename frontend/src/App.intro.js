import logo from './logo.svg';
import './App.css';

function App() {

  //start tmp code
  function getData() {
    fetch('/api/tokens', {
      method: 'POST', 
      headers: {'Authorization': 'Basic ' + btoa('admin:admin')}
    })
    .then(response => response.json())
    .then(json => console.log(json));
  }
  //end tmp code

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p> Test admin login: </p>
        <button onClick={getData}>Click me!</button>
      </header>
    </div>
  );
}

export default App;
