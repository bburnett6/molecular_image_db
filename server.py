#!/usr/bin/env python
import os 
from dbapp import app 

port = os.environ.get('APP_PORT') or 9000
app.run(host="0.0.0.0", port=port)