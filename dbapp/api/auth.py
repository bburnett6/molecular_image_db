
"""auth.py

API authentication functions.
"""
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth
from dbapp.models import User 
from dbapp.api.errors import error_response 

############## Basic

basic_auth = HTTPBasicAuth()

@basic_auth.verify_password 
def verify_password(username, password):
	user = User.query.filter_by(username=username).first()
	if user and user.check_password(password):
		return user 

@basic_auth.error_handler
def basic_auth_error(status):
	return error_response(status)

############ Token 

token_auth = HTTPTokenAuth()

@token_auth.verify_token
def verify_token(token):
	return User.check_token(token) if token else None

@token_auth.error_handler
def token_auth_error(status):
	return error_response(status)