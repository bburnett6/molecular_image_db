
"""users.py

Blueprint routes for the User endpoints of the API
"""
from flask import abort, jsonify, url_for, request

from dbapp import db 
from dbapp.models import User 
from dbapp.api import bp 
from dbapp.api.errors import bad_request
from dbapp.api.auth import token_auth

@bp.route('/users/<int:uid>', methods=['GET'])
@token_auth.login_required
def get_user(uid):
	return jsonify(User.query.get_or_404(uid).to_dict())

@bp.route('/users', methods=['GET'])
@token_auth.login_required
def get_users():
	page = request.args.get('page', 1, type=int)
	per_page = min(request.args.get('per_page', 10, type=int), 100)
	data = User.to_collection_dict(User.query, page, per_page, 'api.get_users')
	return jsonify(data)

@bp.route('/users', methods=['POST'])
def create_user():
	data = request.get_json() or {} 
	if 'username' not in data or 'password' not in data:
		return bad_request('must include username and password!')
	if User.query.filter_by(username=data['username']).first():
		return bad_request('username already in use! Try a different username.')

	user = User()
	user.from_dict(data, new_user=True)
	db.session.add(user)
	db.session.commit()
	response = jsonify(user.to_dict())
	response.status_code = 201 
	response.headers['Location'] = url_for('api.get_user', uid=user.uid)
	return response

@bp.route('/users/<int:uid>', methods=['PUT'])
@token_auth.login_required
def update_user(uid):
	authorized_user = token_auth.current_user()

	if uid == 1:
		return bad_request("No. Don't modify the admin user >:(")

	# if the uid is not the current users uid, assume admin modifications
	if authorized_user.uid != uid:
		# Requires admin permissions to update edit/admin roles
		if not token_auth.current_user().is_admin:
			abort(403)
	else:
		#prevent a random user from updating their own permissions
		if 'edit_permission' in data or 'is_admin' in data:
			return bad_request('only admins can modify these user traits')
	
	#if we made it this far, assume authorized to perform the operation
	user = User.query.get_or_404(uid)
	data = request.get_json() or {}
	user.from_dict(data)
	db.session.add(user)
	db.session.commit()
	response = jsonify(user.to_dict())
	response.status_code = 201
	response.headers['Location'] = url_for('api.update_user', uid=user.uid)
	return response


	