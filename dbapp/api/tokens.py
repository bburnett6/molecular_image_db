
"""tokens.py

Blueprint routes for the Token endpoints of the API.
"""
from flask import jsonify

from dbapp import db 
from dbapp.api import bp 
from dbapp.api.auth import basic_auth, token_auth

@bp.route('/tokens', methods=['POST'])
@basic_auth.login_required
def get_token():
	token = basic_auth.current_user().get_token()
	db.session.commit()
	return jsonify({'uid': basic_auth.current_user().uid, 'token': token})

@bp.route('/tokens', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
	token_auth.current_user().revoke_token()
	db.session.commit()
	return '', 204

