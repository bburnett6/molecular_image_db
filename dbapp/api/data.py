
"""data.py

Blueprint routes for the data endpoints of the API. 
"""
from flask import abort, jsonify, url_for, request
import importlib 
import sys

from dbapp import db
from dbapp.models import DATASETS
#dataset model imports handled within each function to make this script dynamic and short
from dbapp.api import bp 
from dbapp.api.errors import bad_request
from dbapp.api.auth import token_auth

############################
# Create

@bp.route('/data/<dataset>', methods=['POST'])
@token_auth.login_required
def create_dataset(dataset):
	if not token_auth.current_user().edit_permission:
		abort(403)
	data = request.get_json() or {} 
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		#Dynamically create an instance of a class from dbapp.models
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		d = datasetClass()

	# A model class's from_dict checks whether or not a field in the
	# data json is right for the given dataset, enabling a generic 
	# create_dataset
	d.from_dict(data)
	try:
		db.session.add(d)
		db.session.commit()
		response = jsonify(d.to_dict())
		response.status_code = 201
		response.headers['Location'] = url_for('api.get_dataset', dataset=dataset, id=d.id)
		return response
	except Exception as e:
		print(e, file=sys.stderr)
		#The exception occurs at the commit if an add is performed with an object 
		#that already exists. Need to roll back the add, then return an error.
		db.session.rollback() 
		return bad_request("An error occurred entering data into the database. The cause is likely that a column that is required to be unique already exists.")
	

##########################
# Get

@bp.route('/data', methods=['GET'])
@token_auth.login_required
def get_dataset_list():
	# Function to get a list of all queryable datasets
	return jsonify({'datasets': DATASETS})

@bp.route('/data/<dataset>', methods=['GET'])
@token_auth.login_required
def get_datasets(dataset):
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		page = request.args.get('page', 1, type=int)
		per_page = min(request.args.get('per_page', 10, type=int), 100)
		data = datasetClass.to_collection_dict(datasetClass.query, page, per_page, 'api.get_datasets', dataset=dataset)
		return jsonify(data)

@bp.route('/data/<dataset>/count', methods=['GET'])
@token_auth.login_required
def get_dataset_count(dataset):
	"""
	get the number of rows in a dataset. Useful for adding a new
	blank entry to the database.
	"""
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		count = datasetClass.query.count()
		result = {"dataset": dataset, "count": count}
		return jsonify(result)

@bp.route('/data/<dataset>/<int:id>', methods=['GET'])
@token_auth.login_required
def get_dataset(dataset, id):
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		return jsonify(datasetClass.query.get_or_404(id).to_dict())

@bp.route('/data/<dataset>/<query_index>/<query_value>', methods=['GET'])
@token_auth.login_required
def get_dataset_query(dataset, query_index, query_value):
	"""
	get a dataset based on an indexable query. Example, the Sample
	model is indexable on Sample.sampleID. To query on this rather than 
	on Sample.id we would access /data/Sample/sampleID/xxxxxxx
	"""
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		query = {query_index: query_value}
		result = db.one_or_404(
			datasetClass.query.filter_by(**query), #**query unpacks so that it will be filter_by(query_index=query_value)
			description=f"No {dataset} with {query_index}={query_value}"
			)
		return jsonify(result.to_dict())

###########################
# Update

@bp.route('/data/<dataset>/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_dataset(dataset, id):
	if not token_auth.current_user().edit_permission:
		abort(403)
	data = request.get_json() or {}
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		result = datasetClass.query.get_or_404(id)
		result.from_dict(data)
		db.session.add(result)
		db.session.commit()
		response = jsonify(result.to_dict())
		response.status_code = 201
		response.headers['Location'] = url_for('api.get_dataset', dataset=dataset, id=result.id)
		return response

@bp.route('/data/<dataset>/<query_index>/<query_value>', methods=['PUT'])
@token_auth.login_required
def update_dataset_query(dataset, query_index, query_value):
	if not token_auth.current_user().edit_permission:
		abort(403)
	data = request.get_json() or {}
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		query = {query_index: query_value}
		result = db.one_or_404(
			datasetClass.query.filter_by(**query), 
			description=f"No {dataset} with {query_index}={query_value}"
			)
		result.from_dict(data)
		db.session.add(result)
		db.session.commit()
		response = jsonify(result.to_dict())
		response.status_code = 201
		response.headers['Location'] = url_for('api.get_dataset', dataset=dataset, id=result.id)
		return response

#############################
# Delete

@bp.route('/data/<dataset>/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_dataset(dataset, id):
	if not token_auth.current_user().edit_permission:
		abort(403)
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		result = datasetClass.query.get_or_404(id)
		db.session.delete(result)
		db.session.commit()
		return '', 204

@bp.route('/data/<dataset>/<query_index>/<query_value>', methods=['DELETE'])
@token_auth.login_required
def delete_dataset_query(dataset, query_index, query_value):
	if not token_auth.current_user().edit_permission:
		abort(403)
	if dataset not in DATASETS:
		return bad_request('Requested dataset does not exist')
	else:
		datasetClass = getattr(importlib.import_module('dbapp.models'), dataset)
		query = {query_index: query_value}
		result = db.one_or_404(
			datasetClass.query.filter_by(**query),  
			description=f"No {dataset} with {query_index}={query_value}"
			)
		db.session.delete(result)
		db.session.commit()
		return '', 204