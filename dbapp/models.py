
"""models.py

Project wide collection of classes for database representation of
entities. 
"""

import os
import sys
import base64 
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
from flask import url_for 

from dbapp import db 

#################################################
# BEGIN misc helpers

class PaginatedAPIMixin(object):
	"""Paginate a model for browsing via the API

	Keyword Args:
	query - the database query for paginating
	page - The page number. 1 would be the first page and would 
		have per_page entries. EX if there were 100 entries in the query
		and page was 2, then the pagination would display entries 11-20.
	per_page - The number of results to display per page. See page.
	endpoint - The url for retrieving a query.
	"""
	@staticmethod 
	def to_collection_dict(query, page, per_page, endpoint, **kwargs):
		resources = query.paginate(page=page, per_page=per_page, error_out=False)

		data = {
			'items': [item.to_dict() for item in resources.items],
			'_meta': {
				'page': page,
				'per_page': per_page,
				'total_pages': resources.pages,
				'total_items': resources.total,
			},
			'_links': {
				'self': url_for(endpoint, page=page, per_page=per_page, **kwargs),
				'next': url_for(endpoint, page=page + 1, per_page=per_page, 
					**kwargs) if resources.has_next else None,
				'prev': url_for(endpoint, page=page - 1, per_page=per_page, 
					**kwargs) if resources.has_prev else None,
			}
		}

		return data 


"""
DATASETS

A list containing all datasets. When adding a model for the data
endpoint of the API, be sure to add it to this list.
"""
DATASETS = [
	'Genetics',
	'Initials',
	'Sample',
	'Site',
	'Condition',
	'WindDirection',
	'SampleType',
	'SampleDestination',
	'SampleStrategy',
	'Culture',
	'Picking',
	'Species',
	'StorageLoc',
	'StorageBuf',
	'ImgLink',
	'Fluorescence',
	'XRay',
	'SEM',
]

####################################################
# END misc helpers
# BEGIN User

class User(PaginatedAPIMixin, db.Model):
	__bind_key__ = "users"
	uid = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), index=True, unique=True)
	password_hash = db.Column(db.String(64))
	edit_permission = db.Column(db.Boolean)
	is_admin = db.Column(db.Boolean)
	token = db.Column(db.String(32), index=True, unique=True)
	token_expiration = db.Column(db.DateTime)

	def __repr__(self):
		return f'<User {self.username}>'

	def to_dict(self):
		"""present User as dict
		
		Notes:
		* Assume that this is visible by anyone with login permission.
		"""
		data = {
			'uid': self.uid,
			'username': self.username,
			'edit_permission': 'True' if self.edit_permission else 'False',
			'is_admin': 'True' if self.is_admin else 'False',
			'_links': {
				'self': url_for('api.get_user', uid=self.uid),
			}
		}

		return data 

	def from_dict(self, data, new_user=False):
		#for field in ['username', 'edit_permission', 'is_admin']:
		#	if field in data:
		#		setattr(self, field, data[field])
		if 'username' in data:
			setattr(self, 'username', data['username'])
		if 'edit_permission' in data:
			if data['edit_permission'] == 'True': #might come in as string
				setattr(self, 'edit_permission', True)
			else:
				setattr(self, 'edit_permission', False)
		if 'is_admin' in data:
			if data['is_admin'] == 'True':
				setattr(self, 'is_admin', True)
			else:
				setattr(self, 'is_admin', False)
				
		if new_user and 'password' in data:
			self.set_password(data['password'])

	def set_password(self, password):
		self.password_hash = generate_password_hash(password)

	def check_password(self, password):
		return check_password_hash(self.password_hash, password)

	def get_token(self, expires_in=3600):
		"""Retrieve a users API token
		
		Keyword Args:
		expires_in - Expiration time in seconds for the token if one
			needs to be generated. Datatype is Int. 
		"""
		now = datetime.utcnow()
		if self.token and self.token_expiration > now + timedelta(seconds=60):
			return self.token 
		self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
		self.token_expiration = now + timedelta(seconds=expires_in)
		db.session.add(self)

		return self.token 

	def revoke_token(self):
		self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

	@staticmethod
	def check_token(token):
		user = User.query.filter_by(token=token).first()
		if user is None or user.token_expiration < datetime.utcnow():
			return None 
		return user 

################################################## 
# END User
# BEGIN Genetics

class Genetics(PaginatedAPIMixin, db.Model):
	__bind_key__ = "data"
	__tablename__ = "genetics_table"
	id = db.Column(db.Integer, primary_key=True)
	sampleID = db.Column(db.String(24), db.ForeignKey('sample_table.sampleID'))
	sample = db.relationship('Sample', back_populates='genetics', uselist=False)
	sequencingIDF = db.Column(db.String(10), unique=True)
	sequencingIDR = db.Column(db.String(10), unique=True)
	dateSequenced = db.Column(db.DateTime)
	cellCount = db.Column(db.Integer)
	dateExtracted = db.Column(db.DateTime)
	conc = db.Column(db.Float())
	v260280 = db.Column(db.Float())
	v260230 = db.Column(db.Float())
	extractedBy_id = db.Column(db.Integer, db.ForeignKey("initials_table.id"))
	extractedBy = db.relationship('Initials') # array of initials. AKA Extractors
	resultLink = db.Column(db.String(64), unique=True)
	fSeq = db.Column(db.String(3000))
	rSeq = db.Column(db.String(3000))
	mergedSeq = db.Column(db.String(3000))
	mergedTopHitName = db.Column(db.String(64))
	mergedTopHitID = db.Column(db.Float())
	mergedTopHitcov = db.Column(db.Float())

	def __repr__(self):
		return f'<Genetics {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'sampleID': self.sampleID,
			'sequencingIDF': self.sequencingIDF,
			'sequencingIDR': self.sequencingIDR,
			'dateSequenced': self.dateSequenced,
			'cellCount': self.cellCount,
			'dateExtracted': self.dateExtracted,
			'conc': self.conc,
			'260280': self.v260280,
			'260230': self.v260230,
			'extractedBy': self.extractedBy_id,
			'resultLink': self.resultLink,
			'fSeq': self.fSeq,
			'rSeq': self.rSeq,
			'mergedSeq': self.mergedSeq,
			'mergedTopHitName': self.mergedTopHitName,
			'mergedTopHitID': self.mergedTopHitID,
			'mergedTopHitcov': self.mergedTopHitcov,
			#API links
			'_links': {
				'self': 'TODO',
			}
		}

		return data

	def from_dict(self, data):
		relation_attrs = ['sampleID', 'extractedBy']
		datetime_attrs = ['dateSequenced', 'dateExtracted'] 
		attrs = [
			'sequencingIDF', 'sequencingIDR',
			'dateSequenced', 'cellCount',
			'conc',	'v260280', 'v260230',
			'resultLink', 'fSeq', 'rSeq',
			'mergedSeq', 'mergedTopHitName', 'mergedTopHitID',
			'mergedTopHitcov',
		]
		# Add non-relation attributes
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])
		# Datetime objects need to be converted to a python datetime object
		if 'dateSequenced' in data:
			try:
				self.dateSequenced = datetime.strptime(data['dateSequenced'], '%m/%d/%Y') 
			except:
				pass
		if 'dateExtracted' in data:
			try:
				self.dateExtracted = datetime.strptime(data['dateExtracted'], '%m/%d/%Y')
			except:
				pass
		# Need to query for attributes with relations.
		if 'sampleID' in data:
			self.sampleID = Sample.query.filter_by(sampleID=data['sampleID']).first()
		if 'extractedBy' in data:
			new_extractedBy = []
			for initial in data['extractedBy']:
				e = Initals.query.filter_by(id=initial).first()
				if e:
					new_extractedBy.append(e)
			self.extractedBy = new_extractedBy

class Initials(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'initials_table'
	id = db.Column(db.Integer, primary_key=True)
	initials = db.Column(db.String(3), unique=True, index=True)

	def __repr__(self):
		return f'<Initials {self.id}, {self.initials}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'initials': self.initials,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['initials']
		#initials are unique and case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())

######################################### 
# END Genetics
# BEGIN Sample

class Sample(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'sample_table'
	id = db.Column(db.Integer, primary_key=True) 
	genetics = db.relationship('Genetics', back_populates='sample', uselist=False)
	sampleID = db.Column(db.String(24), unique=True, index=True)
	sites_id = db.Column(db.Integer, db.ForeignKey('site_table.id'))
	sites = db.relationship('Site') 
	time = db.Column(db.DateTime)
	date = db.Column(db.DateTime)
	conditions_id = db.Column(db.Integer, db.ForeignKey('condition_table.id'))
	conditions = db.relationship('Condition')
	airTemp = db.Column(db.Integer)
	windSpeed = db.Column(db.Integer)
	windDirections_id = db.Column(db.Integer, db.ForeignKey('wind_direction_table.id'))
	windDirections = db.relationship('WindDirection')
	sampleType_id = db.Column(db.Integer, db.ForeignKey('sample_type_table.id'))
	sampleType = db.relationship('SampleType', uselist=False) 
	latitude = db.Column(db.Float())
	longitude = db.Column(db.Float())
	sampleDest_id = db.Column(db.Integer, db.ForeignKey('sample_destination_table.id'))
	sampleDest = db.relationship('SampleDestination', uselist=False) 
	sampleStrategy_id = db.Column(db.Integer, db.ForeignKey('sample_strategy_table.id'))
	sampleStrategy = db.relationship('SampleStrategy', uselist=False)
	noteField = db.Column(db.String(64))

	@property 
	def GeneticsID(self):
		if self.genetics != None:
			return self.genetics.id 

	def __repr__(self):
		return f'<Sample {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'genetics': self.GeneticsID,
			'sampleID': self.sampleID,
			'sites': self.sites_id,
			'time': self.time,
			'date': self.date,
			'conditions': self.conditions_id,
			'airTemp': self.airTemp,
			'windSpeed': self.windSpeed,
			'windDirections': self.windDirections_id,
			'sampleType': self.sampleType_id,
			'latitude': self.latitude,
			'longitude': self.longitude,
			'sampleDest': self.sampleDest_id,
			'sampleStrategy': self.sampleStrategy_id,
			'noteField': self.noteField,
			#API links
			'_links': {
				'self': 'TODO',
			}
		}

		return data

	def from_dict(self, data):
		relation_attrs = [
			'genetics', 'sites', 
			'conditions', 'windDirections', 'sampleType',
			'sampleDest', 'sampleStrategy',
			]
		datetime_attrs = ['time', 'date']
		attrs = [
		'sampleID', 'airTemp', 'windSpeed', 
		'latitude', 'longitude', 'noteField',
		]
		# Add non-relation attributes
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])
		# Datetime attrs need conversions
		if 'time' in data:
			try:
				self.time = datetime.strptime(data['time'], '%H:%M:%S')  
			except:
				pass
		if 'date' in data:
			try:
				self.date = datetime.strptime(data['date'], '%m/%d/%Y') 
			except:
				pass
		# Now for attributes with relationships
		if 'genetics' in data:
			query = Genetics.query.filter_by(id=data['genetics']).first()
			print(query, file=sys.stderr)
			self.genetics = query
		if 'sites' in data:
			new_sites = []
			for site in data['sites']:
				q = Site.query.filter_by(id=site).first()
				if q:
					new_sites.append(q)
			self.sites = new_sites
		if 'conditions' in data:
			new_conditions = []
			for cond in data['conditions']:
				q = Condition.query.filter_by(id=cond).first()
				if q:
					new_conditions.append(q)
			self.conditions = new_conditions
		if 'windDirections' in data:
			new_windDirections = []
			for wd in data['windDirections']:
				q = WindDirection.query.filter_by(id=wd).first()
				if q:
					new_windDirections.append(q)
			self.windDirections = new_windDirections
		if 'sampleType' in data:
			self.sampleType = SampleType.query.filter_by(id=data['sampleType']).first()
		if 'sampleDest' in data:
			self.sampleDest = SampleDestination.query.filter_by(id=data['sampleDest']).first()
		if 'sampleStrategy' in data:
			self.sampleStrategy = SampleStrategy.query.filter_by(id=data['sampleStrategy']).first()

class Site(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'site_table'
	id = db.Column(db.Integer, primary_key=True)
	marsh = db.Column(db.String(64), unique=True)
	elev = db.Column(db.String(12), unique=True)
	tlp = db.Column(db.String(6), unique=True)

	def __repr__(self):
		return f'<Site {self.id} {self.marsh}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'marsh': self.marsh,
			'elev': self.elev,
			'tlp': self.tlp,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['marsh', 'elev', 'tlp']
		#all attributes should be case insensitive. All get the .lower()
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())

class Condition(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'condition_table'
	id = db.Column(db.Integer, primary_key=True)
	cond = db.Column(db.String(12), unique=True)

	def __repr__(self):
		return f'<Condition {self.id} {self.cond}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'cond': self.cond,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		#cond is case insensitive
		if 'cond' in data:
			setattr(self, 'cond', data['cond'].lower())

class WindDirection(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'wind_direction_table'
	id = db.Column(db.Integer, primary_key=True)
	windDirection = db.Column(db.String(4), unique=True)

	def __repr__(self):
		return f'<WindDirection {self.id} {self.windDirection}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'windDirection': self.windDirection,
			'_links': {
				'self': 'TODO',
			}
		}
		return data

	def from_dict(self, data):
		#'East' is the same as 'east' is the same as 'EaSt' is the same as.....
		if 'windDirection' in data:
			setattr(self, 'windDirection', data['windDirection'].lower())

class SampleType(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'sample_type_table'
	id = db.Column(db.Integer, primary_key=True)
	sampleType = db.Column(db.String(12), unique=True)

	def __repr__(self):
		return f'<SampleType {self.id} {self.sampleType}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'sampleType': self.sampleType,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['sampleType']
		#sample type is unique and case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())

class SampleDestination(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'sample_destination_table'
	id = db.Column(db.Integer, primary_key=True)
	dest = db.Column(db.String(12), unique=True)

	def __repr__(self):
		return f'<SampleDestination {self.id} {self.dest}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'dest': self.dest,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['dest']
		#destination is unique and case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())

class SampleStrategy(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'sample_strategy_table'
	id = db.Column(db.Integer, primary_key=True)
	strat = db.Column(db.String(64))

	def __repr__(self):
		return f'<SampleStrategy {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'strat': self.strat,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['strat']
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])

########################################## 
# END Sample
# BEGIN Culture

class Culture(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'culture_table'
	id = db.Column(db.Integer, primary_key=True)
	cultureName = db.Column(db.String(8), unique=True)
	sampleID = db.Column(db.String(24), db.ForeignKey('sample_table.sampleID'))
	sample = db.relationship('Sample', uselist=False)
	putativeSpecies = db.Column(db.String(24), unique=True)
	dateStarted = db.Column(db.DateTime)

	def __repr__(self):
		return f'<Culture {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'cultureName': self.cultureName,
			'sourceSample': self.sampleID,
			'putativeSpecies': self.putativeSpecies,
			'dateStarted': self.dateStarted,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		relation_attrs = ['sourceSample']
		datetime_attrs = ['dateStarted']
		attrs = ['cultureName', 'putativeSpecies']
		#Both are unique and case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())
		#datetime attributes
		if 'dateStarted' in data:
			try:
				self.dateStarted = datetime.strptime(data['dateStarted'], '%m/%d/%Y')
			except:
				pass
		#Attributes with relationships
		if 'sourceSample' in data:
			self.sample = Sample.query.filter_by(sampleID=data['sourceSample']).first()

################################################
# END Culture
# BEGIN Picking

class Picking(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'picking_table'
	id = db.Column(db.Integer, primary_key=True)
	sampleID = db.Column(db.String(24), db.ForeignKey('sample_table.sampleID'))
	sample = db.relationship('Sample', uselist=False)
	species_id = db.Column(db.Integer, db.ForeignKey('species_table.id'))
	species = db.relationship('Species')
	datePicked = db.Column(db.DateTime)
	storageLoc_id = db.Column(db.Integer, db.ForeignKey('storage_loc_table.id'))
	storageLoc = db.relationship('StorageLoc', uselist=False)
	storageBuf_id = db.Column(db.Integer, db.ForeignKey('storage_buf_table.id'))
	storageBuf = db.relationship('StorageBuf', uselist=False)
	storageVolml = db.Column(db.Integer)
	imgLink_id = db.Column(db.Integer, db.ForeignKey('img_link_table.id'))
	imgLink = db.relationship('ImgLink', uselist=False)
	description = db.Column(db.String(64))

	def __repr__(self):
		return f'<Picking {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'sample': self.sampleID,
			'species': [x.to_dict() for x in self.species],
			'datePicked': self.datePicked,
			'storageLoc': self.storageLoc.to_dict(),
			'storageBuf': self.storageBuf.to_dict(),
			'storageVol_ml': self.storageVolml,
			'imgLink': self.imgLink.to_dict(),
			'description': self.description,
			'_links': {
				'self': 'TODO',
			}
		}
		return data

	def from_dict(self, data):
		relation_attrs = [
		'sample', 'species', 'storageLoc',
		'storageBuf', 'imgLink',
		]
		datetime_attrs = ['datePicked']
		attrs = [ 'storageVol_ml', 'description']
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])
		# Datetime attributes
		if 'datePicked' in data:
			try:
				self.datePicked = datetime.strptime(data['datePicked'], '%m/%d/%Y')
			except:
				pass
		# Attributes with relationships
		if 'sample' in data:
			self.sample = Sample.query.filter_by(sampleID=data['sample']).first()
		if 'species' in data:
			new_species = []
			for s in data['species']:
				q = Species.query.filter_by(id=s).first()
				if q:
					new_species.append(q)
			self.species = new_species
		if 'storageLoc' in data:
			self.storageLoc = StorageLoc.query.filter_by(id=data['storageLoc']).first()
		if 'storageBuf' in data:
			self.storageBuf = StorageBuf.query.filter_by(id=data['storageBuf']).first()
		if 'imgLink' in data:
			self.imgLink = ImgLink.query.filter_by(id=data['imgLink']).first()

class Species(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'species_table'
	id = db.Column(db.Integer, primary_key=True)
	species = db.Column(db.String(32), unique=True)

	def __repr__(self):
		return f'<Species {self.id} {self.species}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'species': self.species,
			'_links': {
				'self': 'TODO',
			}
		}
		return data

	def from_dict(self, data):
		attrs = ['species']
		#species is unique and case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())

class StorageLoc(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'storage_loc_table'
	id = db.Column(db.Integer, primary_key=True)
	location = db.Column(db.String(32), unique=True)

	def __repr__(self):
		return f'<StorageLoc {self.id} {self.location}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'location': self.location,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['location']
		#location is unique and case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())

class StorageBuf(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'storage_buf_table'
	id = db.Column(db.Integer, primary_key=True)
	buffer = db.Column(db.String(64), unique=True)

	def __repr__(self):
		return f'<StorageBuf {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'buffer': self.buffer,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['buffer']
		#buffer is unique and case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr].lower())

class ImgLink(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'img_link_table'
	id = db.Column(db.Integer, primary_key=True)
	link = db.Column(db.String(256), unique=True)

	def __repr__(self):
		return f'<ImgLink {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'link': self.link,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		attrs = ['link']
		#link is unique BUT NOT case insensitive
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])

#############################################
# END Picking
# BEGIN Fluorescence

class Fluorescence(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'fluorescence_table'
	id = db.Column(db.Integer, primary_key=True)
	sampleID = db.Column(db.String(24), db.ForeignKey('sample_table.sampleID'))
	sample = db.relationship("Sample", uselist=False)
	species_id = db.Column(db.Integer, db.ForeignKey('species_table.id'))
	species = db.relationship("Species", uselist=False)
	link = db.Column(db.String(256), unique=True)

	def __repr__(self):
		return f'<Fluorescence {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'sampleID': self.sampleID, 
			'species': self.species.to_dict(),
			'link': self.link,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		relation_attrs = ['sampleID', 'species']
		attrs = ['link']
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])
		# Attributes with relationships
		if 'sampleID' in data:
			self.sample = Sample.query.filter_by(sampleID=data['sampleID']).first()
		if 'species' in data:
			self.species = Species.query.filter_by(id=data['species']).first()

##################################################
# END Fluorescence
# BEGIN XRay

class XRay(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'xray_table'
	id = db.Column(db.Integer, primary_key=True)
	sampleID = db.Column(db.String(24), db.ForeignKey('sample_table.sampleID'))
	sample = db.relationship("Sample", uselist=False)
	species_id = db.Column(db.Integer, db.ForeignKey('species_table.id'))
	species = db.relationship("Species", uselist=False)
	ph = db.Column(db.Float())
	link = db.Column(db.String(256), unique=True)

	def __repr__(self):
		return f'<XRay {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'sampleID': self.sampleID,
			'species': self.species.to_dict(),
			'ph': self.ph,
			'link': self.link,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		relation_attrs = ['sampleID', 'species']
		attrs = ['ph', 'link']
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])
		# Attributes with relationships
		if 'sampleID' in data:
			self.sample = Sample.query.filter_by(sampleID=data['sampleID']).first()
		if 'species' in data:
			self.species = Species.query.filter_by(id=data['species']).first()


#####################################################
# END XRay
# BEGIN SEM

class SEM(PaginatedAPIMixin, db.Model):
	__bind_key__ = 'data'
	__tablename__ = 'sem_table'
	id = db.Column(db.Integer, primary_key=True)
	sampleID = db.Column(db.String(24), db.ForeignKey('sample_table.sampleID'))
	sample = db.relationship("Sample", uselist=False)
	species_id = db.Column(db.Integer, db.ForeignKey('species_table.id'))
	species = db.relationship("Species", uselist=False)
	genetics_id = db.Column(db.Integer, db.ForeignKey('genetics_table.id'))
	genetics = db.relationship("Genetics", uselist=False)
	link = db.Column(db.String(256), unique=True)

	def __repr__(self):
		return f'<SEM {self.id}>'

	def to_dict(self):
		data = {
			'id': self.id,
			'sampleID': self.sampleID,
			'species': self.species.to_dict(),
			'geneticsID': self.genetics_id,
			'link': self.link,
			'_links': {
				'self': 'TODO',
			}
		}
		return data 

	def from_dict(self, data):
		relation_attrs = ['sampleID', 'species', 'genetics']
		attrs = ['link']
		for attr in attrs:
			if attr in data:
				setattr(self, attr, data[attr])
		# Attributes with relationships
		if 'sampleID' in data:
			self.sample = Sample.query.filter_by(sampleID=data['sampleID']).first()
		if 'species' in data:
			self.species = Species.query.filter_by(id=data['species']).first()
		if 'genetics' in data:
			self.genetics = Genetics.query.filter_by(id=data['genetics']).first()


##########################################
# END SEM