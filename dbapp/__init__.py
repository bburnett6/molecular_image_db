
import os
import sys

from flask import Flask 
from dbapp.config import Config 
from flask_sqlalchemy import SQLAlchemy 
import flask_migrate

app = Flask(__name__) 

app.config.from_object(Config())

db = SQLAlchemy(app)
mdir = os.path.join(os.environ.get('MIGRATIONS_DIR') or os.getcwd(), 'migrations')
migrate = flask_migrate.Migrate(app, db, directory=mdir)

from dbapp import models

#if not os.path.exists(mdir):
#initialize the db
"""
This procedure is equivalent to doing the following cli routine
```
flask --app dbapp db init
flask --app dbapp db migrate -m "Initial migration"
flask --app dbapp db upgrade
```
Once initialized and if changes are made to any models in models.py
migrations can be made programatically by running migrate() and upgrade()
or their respective CLI alternatives.
"""
with app.app_context(): 
	if not os.path.exists(mdir):
		print("Init", file=sys.stderr)
		flask_migrate.init(directory=mdir, multidb=True)
		print("Migrate", file=sys.stderr)
		flask_migrate.migrate(directory=mdir, message='Initial migration')
	print("Upgrade", file=sys.stderr)
	flask_migrate.upgrade(directory=mdir)
	print("Create", file=sys.stderr)
	db.create_all()
	print("Admin", file=sys.stderr)
	admin_user = models.User.query.filter_by(uid=1).first()
	if not admin_user:
		first_admin = models.User(username='admin', edit_permission=True, is_admin=True)
		first_admin.set_password('admin')
		db.session.add(first_admin)
		db.session.commit()

from dbapp.api import bp as api_bp 
app.register_blueprint(api_bp, url_prefix='/api')
