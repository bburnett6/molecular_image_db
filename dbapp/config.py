
import os 

basedir = os.path.abspath(os.path.dirname(__file__))
userdb = os.environ.get('USER_DATABASE_URL') or f"sqlite:///{os.path.join(basedir, 'users.db')}"
datadb = os.environ.get('DATA_DATABASE_URL') or f"sqlite:///{os.path.join(basedir, 'data.db')}"

class Config(object):

	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
		f"sqlite:///{os.path.join(basedir, 'app.db')}"
	SQLALCHEMY_BINDS = {
		"users": userdb,
		"data": datadb,
	}
	SQLALCHEMY_TRACK_MODIFICATIONS = True